# Frisbee Saleor (Back-end) Challenge

Te presentamos nuestra evaluación para candidatos como Back-end Engineer en [Frisbee App](https://frisbee.com.ec).


## De qué se trata esta evaluación?

Queremos darte el tiempo y la libertad de mostrarnos tus skills con un proyecto real. No creemos que la mejor forma de evaluar a un candidato es mediante una entrevista con presión de tiempo, temas desconocidos o rompecabezas matemáticos que no son relevantes para un/a miembro/a del Equipo de Ingenieria en Frisbee.

Diseñamos esta evaluación con la intención de hacer una evaluación real y poder discutir tu solución e implementación más a fondo en nuestra siguiente entrevista.

Creemos que el desafío no debería demorarte más de un día para completar, asi que utiliza eso como una métrica de tu tiempo.

El proyecto se basa en la última versión de [Saleor](https://github.com/mirumee/saleor/pull/5021/files) y debemos agregar una nueva funcionalidad: Reviews

Esperamos la disfrutes!

## Tecnologías:
- Python 3.6
- Django
- GraphQL
## Desafío

Queremos utilizar la API de Saleor, si no conoces de este proyecto, puedes ver más en la documentación oficial [aquí](https://docs.saleor.io/).

Vamos a utilizar esta API para agregar una nueva funcionalidad que creemos que es muy importante para todos los eCommerce, la opción de agregar una "Review" a un producto.


El desafío es crear 2 endpoints en GraphQL para permitir crear y ver Reviews en los productos que tenemos en Saleor.

## Funcionalidad

### Parte #1: Crear Review:

- Una Review tiene que ser específica a un producto, tiene que ser creada por un usuario o un usuario anónimo.

- Cada Review tiene que tener: un puntaje (1 a 5), una comentario (texto), fecha de creacion, fecha de modificación

- OPCIONAL: Cada Review puede tener una o más fotos.

- Tenemos que crear un GraphQL Mutation que permita a un usuario crear la Review.


### Parte #2 Leer Reviews de Productos:

- Crear un GraphQL query para poder ver todos los Reviews de un producto

- Permitir que la Query pueda tener paginación, es decir que se pueda filtrar los Reviews del producto en un tiempo específico de fechas, número máximo de reviews y filtro por puntaje.

### Parte #3 Leer Reviews de Usuarios:


### Instrucciones

Clona el repositorio
```
git clone git@github.com:mirumee/saleor.git
```

Abre tu rama de Git
```
git checkout -b <nombre>-<apellido>
```

Comienza el desarrollo y sigue todas las instrucciones para correr el proyecto de Saleor localmente.


## Evaluación
- 50% - Diseño General y Estructura
- 50% - Data Management y Algoritmos



## Cómo Enviar tu Solución:

Crea una branch en tu proyecto local con el siguiente formato `nombre-apellido`, después de revisarlo juntos en la siguiente entrevista vas a poder subirlo al repositorio remoto. 